<?php

namespace Integration\Shared\Domain;

use Exception;
use Integration\PromoDJ\Application\Integration as PromoDJIntegration;
use Integration\Shared\Application\IntegrationInterface;
use Shared\Domain\Enum\IntegrationEnum;
use Shared\Infrastructure\Service\ServiceInterface;

class IntegrationFactory implements ServiceInterface
{

    private const MESSAGE_NOT_VALID_MAPPING = 'Не найден маппинг для интеграции %s';

    public function __construct(
        private IntegrationEnum $enum,
    ) {}

    /**
     * @throws Exception
     */
    public function service(): IntegrationInterface
    {
        return $this->getIntegration();
    }

    /**
     * @throws Exception
     */
    private function getIntegration(): IntegrationInterface
    {
        $integrationClass = match ($this->enum) {
            IntegrationEnum::PromoDJ => PromoDJIntegration::class,
            default => throw new Exception(sprintf(self::MESSAGE_NOT_VALID_MAPPING, $this->enum->value)),
        };

        return new $integrationClass();
    }
}