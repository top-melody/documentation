<?php

namespace Integration\Shared\Domain\ApiOperation;

use Shared\Domain\Enum\ApiOperationTypeEnum;
use Shared\Domain\Enum\IntegrationEnum;
use Shared\Infrastructure\Service\ServiceInterface;
use Exception;

class ApiOperationBuilder implements ServiceInterface
{

    private ?IntegrationEnum $integration = null;
    private ?ApiOperationTypeEnum $type = null;

    private ?string $requestBuilderClass = null;
    private ?string $senderClass = null;
    private ?string $errorsAnalyzerClass = null;
    private ?string $resultBuilderClass = null;

    private array $requestBuilderDependencies = [];
    private array $senderDependencies = [];
    private array $errorsAnalyzerDependencies = [];
    private array $resultBuilderDependencies = [];

    public static function create(): self
    {
        return new self();
    }

    public function setIntegration(IntegrationEnum $integration): self
    {
        $this->integration = $integration;

        return $this;
    }

    public function setType(ApiOperationTypeEnum $type): self
    {
        $this->type = $type;

        return $this;
    }

    public function setRequestBuilder(string $class, mixed ...$dependencies): self
    {
        $this->requestBuilderClass = $class;
        $this->requestBuilderDependencies = $dependencies;

        return $this;
    }

    public function setSender(string $class, mixed ...$dependencies): self
    {
        $this->senderClass = $class;
        $this->senderDependencies = $dependencies;

        return $this;
    }

    public function setErrorsAnalyzer(string $class, mixed ...$dependencies): self
    {
        $this->errorsAnalyzerClass = $class;
        $this->errorsAnalyzerDependencies = $dependencies;

        return $this;
    }

    public function setResultBuilder(string $class, mixed ...$dependencies): self
    {
        $this->resultBuilderClass = $class;
        $this->resultBuilderDependencies = $dependencies;

        return $this;
    }

    private function hasRequiredParams(): bool
    {
        return !is_null($this->integration)
            && !is_null($this->type)
            && !is_null($this->requestBuilderClass)
            && !is_null($this->senderClass)
            && !is_null($this->resultBuilderClass);
    }

    /**
     * @throws Exception
     */
    public function service(): ApiOperation
    {
        if (!$this->hasRequiredParams()) {
            throw new Exception('Обязательные зависимости не определены');
        }

        return new ApiOperation(
            $this->integration,
            $this->type,
            $this->requestBuilderClass,
            $this->senderClass,
            $this->errorsAnalyzerClass,
            $this->resultBuilderClass,
            $this->requestBuilderDependencies,
            $this->senderDependencies,
            $this->errorsAnalyzerDependencies,
            $this->resultBuilderDependencies,
        );
    }
}