<?php

namespace Integration\Shared\Application;

use DateTimeInterface;
use Exception;
use http\Encoding\Stream;
use Integration\Shared\Domain\IntegrationFactory;
use Shared\Domain\Enum\IntegrationEnum;

class IntegrationDecorator implements IntegrationInterface
{

    private IntegrationInterface $integration;

    /**
     * @throws Exception
     */
    public function __construct(IntegrationEnum $enum)
    {
        $factory = new IntegrationFactory($enum);
        $this->integration = $factory->service();
    }

    public function getTrackListByDates(DateTimeInterface $dateFrom, DateTimeInterface $dateTo): array
    {
        return $this->integration->getTrackListByDates($dateFrom, $dateTo);
    }

    public function getTrackListByUserUrl(string $userUrl): array
    {
        return $this->integration->getTrackListByUserUrl($userUrl);
    }

    public function getTrackStream(string $downloadUrl): Stream
    {
        return $this->integration->getTrackStream($downloadUrl);
    }

    public function downloadTrack(string $downloadUrl, string $path): void
    {
        $this->integration->downloadTrack($downloadUrl, $path);
    }
}