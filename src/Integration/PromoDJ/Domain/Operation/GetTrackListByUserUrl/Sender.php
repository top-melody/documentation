<?php

namespace Integration\Promodj\Domain\Operation\GetTrackListByUserUrl;

use SDK\Client;
use SDK\Request;
use SDK\Response;
use Shared\Infrastructure\Service\ServiceInterface;

class Sender implements ServiceInterface
{

    public function __construct(
        private Request $request,
        private Client $client,
    ) {}

    public function service(): Response
    {
        return $this->client->send($this->request);
    }
}