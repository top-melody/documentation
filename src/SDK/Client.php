<?php

namespace SDK;

class Client
{
    public function send(Request $request): Response
    {
        $response = new Response();
        $response->success = true;
        $response->trackList = [];

        return $response;
    }
}