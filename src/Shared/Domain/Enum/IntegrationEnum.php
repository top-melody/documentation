<?php

namespace Shared\Domain\Enum;

enum IntegrationEnum: string
{
    case PromoDJ = 'PromoDJ';
    case VK = 'VK';
}